# ISS tracker

```cmd
$ ./iss_tracker --help
Usage: iss_tracker [OPTIONS] --iss-path <ISS_PATH> --areas-path <AREAS_PATH>

Options:
  -i, --iss-path <ISS_PATH>      ISS data file path
  -a, --areas-path <AREAS_PATH>  Tracking area file path
  -o, --output-dir <OUTPUT_DIR>  Output directory [default: iss_traces]
  -h, --help                     Print help
  -V, --version                  Print version
```