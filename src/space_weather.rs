use std::{
    collections::BTreeMap,
    fs::File,
    io::{BufRead, BufReader},
    num::ParseIntError,
    ops::{Bound, RangeBounds},
};

use chrono::{NaiveDate, NaiveDateTime};
use ionos::FromFileByLines;
use thiserror::Error;

/// in seconds
pub const KP_TIME_STEP: i64 = 3600 * 3;

#[derive(Debug, Error)]
pub enum SpaceWeatherError {
    #[error("Invalid date unit: {0:?}")]
    InvalidDateUnit(#[from] ParseIntError),
    #[error("Invalid date")]
    InvalidDate,
    #[error("")]
    IO(#[from] std::io::Error),
}

#[derive(Debug)]
pub struct SpaceWeather {
    kps: BTreeMap<NaiveDateTime, f64>,
}

impl SpaceWeather {
    pub fn kp_range<R>(&self, range: R) -> std::collections::btree_map::Range<NaiveDateTime, f64>
    where
        R: RangeBounds<NaiveDateTime>,
    {
        let range = (
            self.get_start_bound(range.start_bound()),
            range.end_bound().cloned(),
        );

        self.kps.range(range)
    }

    fn get_start_bound(&self, bound: Bound<&NaiveDateTime>) -> Bound<NaiveDateTime> {
        match bound {
            Bound::Included(v) => {
                let timestamp = v.timestamp();

                Bound::Included(
                    NaiveDateTime::from_timestamp_opt(timestamp - timestamp % KP_TIME_STEP, 0)
                        .unwrap(),
                )
            }
            Bound::Excluded(v) => {
                let timestamp = v.timestamp();

                Bound::Included(
                    NaiveDateTime::from_timestamp_opt(timestamp - timestamp % KP_TIME_STEP, 0)
                        .unwrap(),
                )
            }
            Bound::Unbounded => Bound::Unbounded,
        }
    }
}

impl FromFileByLines for SpaceWeather {
    type Err = SpaceWeatherError;

    type Item = BTreeMap<NaiveDateTime, f64>;

    type Target = SpaceWeather;

    fn parse_item_str(record: &str) -> Result<Self::Item, Self::Err> {
        let split_iter = record.split_ascii_whitespace();
        let date = split_iter.clone().take(3).collect::<Vec<_>>();
        let date = NaiveDate::from_ymd_opt(date[0].parse()?, date[1].parse()?, date[2].parse()?)
            .ok_or(SpaceWeatherError::InvalidDate)?;

        let map = split_iter
            .skip(7)
            .take(8)
            .enumerate()
            .map(|(idx, x)| {
                let datetime = date
                    .and_hms_opt(idx as u32 * 3, 0, 0)
                    .ok_or(SpaceWeatherError::InvalidDate)
                    .unwrap();

                (datetime, x.parse::<f64>().unwrap())
            })
            .collect::<BTreeMap<_, _>>();

        Ok(map)
    }

    fn from_file_by_lines<P: AsRef<std::path::Path>>(path: P) -> Result<Self::Target, Self::Err> {
        let mut kps = BTreeMap::default();

        let file = File::open(path).map_err(SpaceWeatherError::IO)?;

        BufReader::new(file).lines().skip(40).for_each(|line| {
            kps.extend(Self::parse_item_str(&line.unwrap()).unwrap());
        });

        Ok(SpaceWeather { kps })
    }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use float_cmp::approx_eq;

    use super::*;

    const DEFAULT_ULPS: i64 = 6;

    #[test]
    fn from_file_by_lines() {
        let dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        let path = dir.join("test/Kp_ap_Ap_SN_F107_2021.txt");

        let expected = [
            [0.000, 0.333, 0.667, 0.333, 0.333, 1.333, 0.667, 0.667],
            [0.333, 0.000, 0.000, 0.333, 0.000, 0.000, 0.000, 0.000],
        ]
        .iter()
        .enumerate()
        .flat_map(|(di, kps)| {
            let date = NaiveDate::from_ymd_opt(2021, 1, di as u32 + 1).unwrap();

            kps.iter()
                .enumerate()
                .map(|(idx, &x)| {
                    let datetime = date
                        .and_hms_opt(idx as u32 * 3, 0, 0)
                        .ok_or(SpaceWeatherError::InvalidDate)
                        .unwrap();

                    (datetime, x)
                })
                .collect::<Vec<_>>()
        })
        .collect::<BTreeMap<_, _>>();

        let actual = SpaceWeather::from_file_by_lines(path).unwrap();
        let actual = actual.kps;

        assert_eq!(expected.len(), actual.len());
        for (k, v) in expected {
            let av = *actual.get(&k).unwrap();

            approx_eq!(f64, v, av, ulps = DEFAULT_ULPS);
        }
    }
}
