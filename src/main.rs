use std::{ops::RangeBounds, path::Path};

use areas::AreaReader;
use chrono::NaiveDateTime;
use clap::{command, Parser};
use ionos::{
    space_station::{area::AreaTrait, StationRecord, Trace},
    FromFileByLines,
};
use space_weather::{SpaceWeather, KP_TIME_STEP};
use station_service::{write_in_file, StationService};

mod areas;
mod space_weather;
mod station_service;

const KP_THRESHOLD: f64 = 2.67;

#[derive(Parser, Debug)]
#[command(author = "Roman Shabanov", version, about, long_about = None)]
struct Args {
    /// ISS data file path
    #[arg(short = 'i', long)]
    iss: String,

    /// Tracking area file path
    #[arg(short = 'a', long)]
    areas: String,

    /// Space weather file (optional)
    #[arg(short = 's', long)]
    space_weather: Option<String>,

    /// Output directory
    #[arg(short = 'o', long, default_value = "iss_traces")]
    output_dir: String,
}

fn main() -> Result<(), anyhow::Error> {
    let args = Args::parse();

    let station = StationService::from_file_by_lines(&args.iss)?;
    let weather = if let Some(space_weather_path) = args.space_weather {
        Some(SpaceWeather::from_file_by_lines(space_weather_path)?)
    } else {
        None
    };

    let file_name = Path::new(&args.iss).file_stem().unwrap();
    let output_dir = Path::new(&args.output_dir).join(file_name);

    let area_reader = AreaReader;
    let areas = area_reader.areas(&args.areas)?;

    areas.iter().for_each(|(place, area)| {
        let rect_traces = station.traces_in_area(&area.rect);

        let records = rect_traces
            .filter(|trace| {
                trace
                    .clone()
                    .any(|x| area.ellipse.contains(x.get_location()))
            })
            .fold(Vec::default(), |mut acc, trace| {
                if let (Some(first), Some(last)) = (trace.clone().next(), trace.clone().last()) {
                    let iter = weather.as_ref().map_or_else(
                        || trace.clone().collect::<Vec<_>>().into_iter(),
                        |weather| {
                            filter_trace_by_kp(
                                trace.clone(),
                                weather,
                                first.get_datetime()..last.get_datetime(),
                            )
                        },
                    );

                    acc.push(iter);
                }

                acc
            })
            .into_iter()
            .flat_map(|x| x.collect::<Vec<_>>());

        write_in_file(place, records, output_dir.as_os_str());
    });

    Ok(())
}

fn filter_trace_by_kp<'a, R>(
    trace: Trace<'a>,
    weather: &SpaceWeather,
    range: R,
) -> std::vec::IntoIter<&'a StationRecord>
where
    R: RangeBounds<NaiveDateTime>,
{
    weather
        .kp_range(range)
        .filter(|(_, kp)| **kp <= KP_THRESHOLD)
        .fold(Vec::default(), |mut records, (datetime, _)| {
            let datetime =
                NaiveDateTime::from_timestamp_opt(datetime.timestamp_millis() + KP_TIME_STEP, 0)
                    .unwrap();

            records.extend(
                trace
                    .clone()
                    .skip(records.len())
                    .take_while(|record| datetime.gt(record.get_datetime())),
            );

            records
        })
        .into_iter()
}
