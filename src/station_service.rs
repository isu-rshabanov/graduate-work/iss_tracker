use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

use chrono::NaiveDateTime;

use ionos::{
    geom::Point,
    space_station::{station_record::StationRecord, ParseErrorKind, Station, StationError},
    FromFileByLines,
};

const DATETIME_FORMAT: &str = "%Y%m%d\t%H%M%S";

pub struct StationService;

impl FromFileByLines for StationService {
    type Err = StationError;

    type Item = StationRecord;

    type Target = Station;

    fn parse_item_str(record: &str) -> Result<Self::Item, Self::Err> {
        let iter = record.rsplitn(4, char::is_whitespace);
        let datetime = iter
            .clone()
            .last()
            .map(|x| {
                NaiveDateTime::parse_from_str(x, DATETIME_FORMAT)
                    .map_err(ParseErrorKind::InvalidDatetime)
            })
            .unwrap()?;
        let data = iter
            .take(3)
            .map(|x| {
                x.parse::<f64>()
                    .map_err(|_| StationError::Parse(ParseErrorKind::InvalidLocation))
                    .unwrap()
            })
            .collect::<Vec<_>>();

        let location = Point::new(data[2], data[1], data[0]);

        Ok(StationRecord::new(datetime, location))
    }

    fn from_file_by_lines<P: AsRef<Path>>(path: P) -> Result<Self::Target, Self::Err> {
        let file = File::open(path).map_err(StationError::IO)?;
        let lines = BufReader::new(file).lines().skip(1);

        let mut records = if let (_, Some(capacity)) = lines.size_hint() {
            Vec::with_capacity(capacity)
        } else {
            Vec::default()
        };

        for line in lines {
            let line = line.map_err(StationError::IO)?;
            records.push(Self::parse_item_str(&line)?);
        }

        Ok(records.into_iter().collect::<Station>())
    }
}

pub fn write_in_file<'a, T>(place: &str, records: impl Iterator<Item = &'a StationRecord>, dir: T)
where
    T: AsRef<Path>,
{
    let mut contents = Vec::from(["YYMMDD\tHHMMSS\tLatitude\tLongitude\tAltitude(km)".to_owned()]);
    contents.extend(records.map(|record: &StationRecord| {
        let location = record.get_location();
        format!(
            "{}\t{:.6}\t{:.6}\t{:.6}",
            record.get_datetime().format(DATETIME_FORMAT),
            location.x,
            location.y,
            location.z
        )
    }));

    if !dir.as_ref().is_dir() {
        std::fs::create_dir_all(dir.as_ref()).unwrap();
    }

    let filepath = dir.as_ref().join(format!("{place}.dat"));
    std::fs::write(filepath, contents.join("\n")).unwrap();
}
