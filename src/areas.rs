use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

use float_cmp::approx_eq;
use ionos::{
    geom::{Coordinate, Degree, Distance, Point},
    space_station::area::AreaTrait,
};
use thiserror::Error;

const DEFAULT_ULPS: i64 = 6;

#[derive(Debug, PartialEq, Eq)]
pub struct Area {
    pub rect: RectArea,
    pub ellipse: EllipseRectArea,
}

#[derive(Debug, PartialEq, Eq)]
pub struct RectArea {
    lt_apex: Point,
    rb_apex: Point,
}

#[derive(Debug)]
pub struct EllipseRectArea {
    center: Point,
    xr: Distance,
    yr: Distance,
    rect: RectArea,
}

impl Eq for EllipseRectArea {}

impl PartialEq for EllipseRectArea {
    fn eq(&self, other: &Self) -> bool {
        self.center == other.center
            && approx_eq!(Distance, self.xr, other.xr, ulps = DEFAULT_ULPS)
            && approx_eq!(Distance, self.yr, other.yr, ulps = DEFAULT_ULPS)
            && self.rect == other.rect
    }
}

impl AreaTrait for RectArea {
    fn contains(&self, point: &Point) -> bool {
        point.x >= self.lt_apex.x
            && point.x <= self.rb_apex.x
            && point.y >= self.lt_apex.y
            && point.y <= self.rb_apex.y
    }
}

impl AreaTrait for EllipseRectArea {
    fn contains(&self, point: &Point) -> bool {
        if !self.rect.contains(point) {
            return false;
        }

        let x = ((point.x - self.center.x) / self.xr).powi(2);
        let y = ((point.y - self.center.y) / self.yr).powi(2);

        x + y <= 1.0
    }
}

#[derive(Debug, Error)]
pub enum AreaReaderError {
    #[error("{0:?}")]
    IOError(#[from] std::io::Error),
    #[error("invalid string: {0:?} ")]
    InvalidStr(String),
}

pub type AreaReaderResult<T> = Result<T, AreaReaderError>;

pub struct AreaReader;

impl AreaReader {
    pub fn areas<P>(&self, path: P) -> AreaReaderResult<HashMap<String, Area>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(path)?;
        let reader = BufReader::new(file);

        let mut areas = HashMap::default();

        for line in reader.lines() {
            let (place, area) = self.parse_str(&line?)?;
            areas.insert(place, area);
        }

        Ok(areas)
    }

    fn parse_str(&self, text: &str) -> AreaReaderResult<(String, Area)> {
        const PATTERN: &str =
            r"(?P<place>[\w-]+?):\s(?P<area>\d+[NS]?[\s-]+\d+[NS][\s,]*\d+[EW]?[\s-]+\d+[EW])";
        let re = regex::Regex::new(PATTERN).unwrap();

        let captures = re
            .captures_iter(text)
            .next()
            .ok_or(AreaReaderError::InvalidStr(text.to_string()))?;

        let place = captures["place"].to_owned();
        let area = self.parse_area(&captures["area"])?;

        Ok((place, area))
    }

    fn parse_area(&self, text: &str) -> AreaReaderResult<Area> {
        let rect = self.parse_rect_area(&text.to_uppercase())?;
        let ellipse = self.create_ellipse_from_rect(&rect);

        Ok(Area { rect, ellipse })
    }

    fn parse_rect_area(&self, text: &str) -> AreaReaderResult<RectArea> {
        const DEFAULT_ALTITUDE: Coordinate = 0.0;
        const PATTERN: &str = r"(\d+)([NS])?[\s-]+(\d+)([NS])[\s,]*(\d+)([EW])?[\s-]+(\d+)([EW])";
        let re = regex::Regex::new(PATTERN).unwrap();

        let captures = re
            .captures(text)
            .ok_or(AreaReaderError::InvalidStr(text.to_owned()))?;

        let mut coords = Vec::with_capacity(4);
        for idx in (1..8).step_by(2) {
            let degree = captures
                .get(idx)
                .unwrap()
                .as_str()
                .parse::<Degree>()
                .unwrap();

            if let Some(dir) = &captures.get(idx + 1) {
                let dir = dir.as_str();
                if idx < 4 && !self.is_latitude(dir) || idx >= 4 && !self.is_longitude(dir) {
                    panic!("Undefined behavior: unknown cardinal direction");
                }

                coords.push(degree * self.get_dir_sign(dir));
            } else {
                coords.push(degree);
            }
        }

        Ok(RectArea {
            lt_apex: Point::new(coords[0], coords[2], DEFAULT_ALTITUDE),
            rb_apex: Point::new(coords[1], coords[3], DEFAULT_ALTITUDE),
        })
    }

    #[inline(always)]
    fn is_latitude(&self, dir: &str) -> bool {
        ["N", "S"].contains(&dir.to_uppercase().as_str())
    }

    #[inline(always)]
    fn is_longitude(&self, dir: &str) -> bool {
        ["E", "W"].contains(&dir.to_uppercase().as_str())
    }

    #[inline(always)]
    fn get_dir_sign(&self, text: &str) -> Degree {
        match text {
            "N" | "E" => 1.0,
            "S" | "W" => -1.0,
            _ => panic!("Undefined cardinal direction: {text}"),
        }
    }

    fn create_ellipse_from_rect(&self, rect: &RectArea) -> EllipseRectArea {
        const DEFAULT_ALTITUDE: Coordinate = 0.0;
        let center = Point::new(
            (rect.lt_apex.x + rect.rb_apex.x) / 2.0,
            (rect.lt_apex.y + rect.rb_apex.y) / 2.0,
            DEFAULT_ALTITUDE,
        );

        let xr = (rect.rb_apex.x - center.x) / 3.0;
        let yr = (rect.rb_apex.y - center.y) / 3.0;

        let ellipse_rect = RectArea {
            lt_apex: Point::new(center.x - xr, center.y - yr, DEFAULT_ALTITUDE),
            rb_apex: Point::new(center.x + xr, center.y + yr, DEFAULT_ALTITUDE),
        };

        EllipseRectArea {
            center,
            xr,
            yr,
            rect: ellipse_rect,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use super::*;

    #[test]
    fn areas_test() {
        let dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        let path = dir.join("test/areas.txt");
        let expected_areas = HashMap::from([(
            "Япония".to_owned(),
            Area {
                rect: RectArea {
                    lt_apex: (20.0, 120.0, 0.0).into(),
                    rb_apex: (50.0, 150.0, 0.0).into(),
                },
                ellipse: EllipseRectArea {
                    center: (35.0, 135.0, 0.0).into(),
                    xr: 5.0,
                    yr: 5.0,
                    rect: RectArea {
                        lt_apex: (30.0, 130.0, 0.0).into(),
                        rb_apex: (40.0, 140.0, 0.0).into(),
                    },
                },
            },
        )]);

        let actual_areas = AreaReader.areas(path).unwrap();

        assert_eq!(
            expected_areas.keys().collect::<Vec<_>>(),
            actual_areas.keys().collect::<Vec<_>>()
        );

        for (place, area) in expected_areas {
            assert_eq!(area, actual_areas[&place])
        }
    }

    #[test]
    fn invalid_str_test() {
        const INVALID_STR: &str = "Япония: 20-50N, 120-150";

        let result = AreaReader.parse_str(INVALID_STR);

        assert!(matches!(result, Err(AreaReaderError::InvalidStr(_))));
    }

    #[test]
    fn invalid_path_test() {
        let result = AreaReader.areas("");

        assert!(matches!(result, Err(AreaReaderError::IOError(_))));
    }
}
